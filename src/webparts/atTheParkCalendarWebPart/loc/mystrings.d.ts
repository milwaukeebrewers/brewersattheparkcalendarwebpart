declare interface IAtTheParkCalendarWebPartWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'AtTheParkCalendarWebPartWebPartStrings' {
  const strings: IAtTheParkCalendarWebPartWebPartStrings;
  export = strings;
}
