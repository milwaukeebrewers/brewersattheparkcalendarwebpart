import * as React from 'react';
import * as moment from 'moment';
import {CalendarProps} from './ICalendarProps';
import CalendarDateCard from './CalendarDateCard';
import CalendarHeader from './CalendarHeader';

export default class Calendar extends React.Component<CalendarProps, {}>  
{
    constructor(props){
        super(props);
    }
    private calendarStyle={
        backgroundColor: 'transparent',
        width: 600,
        padding: '10px 10px 10px 15px', 
        boxShadow: "0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1)",
        
    };
    private weekdays= moment.weekdays();
    private weekdaysShort = moment.weekdaysShort();
    private months = moment.months();
    private year = () =>{
        return this.props.selectedDate.format("Y");
    }
    private month = () =>{
        return this.props.selectedDate.format("MMMM");
    }
    private daysInMonth = () => {
        return this.props.selectedDate.daysInMonth();
    }
    private currentDay = () =>{
        return this.props.selectedDate.format("D");
    }
    private firstDayOfMonth = () => {
        let selectedDate = this.props.selectedDate;
        return parseInt(moment(selectedDate).startOf('month').format('d'));
    }
    public render(){
        let weekdays = this.weekdaysShort.map((day)=>
        {
            return(<th key={day} className="week-day">{day}</th>);
        });
        let calendarSpots = [];
        for(let i = 0; i < this.firstDayOfMonth(); i++){
            calendarSpots.push(
            <td key={i*87} style={{width: 83,padding: '0px', margin:'0px', border: 'solid #d3d3d3', borderWidth: '1px', height: '100%'}}>
                {""}
            </td>
            );
        }
        for(let j = 1; j <= this.daysInMonth(); j++){
            calendarSpots.push(
                <td key={j} style={{width: 83,padding: '0px', margin:'0px', border: 'solid #d3d3d3', borderWidth: '1px', height: '100%', backgroundColor: (this.props.calendarList[j-1].game === undefined || this.props.calendarList[j-1].game === null) ? 'white' : 'rgba(0,0,0,.75)'}}>
                    <CalendarDateCard dateItem={this.props.calendarList[j-1]} selectDateFunc={this.props.selectDateFunc}/>
                </td>
            );
        }
        let rows = [];
        let cells = [];
        calendarSpots.forEach((spot, i)=>{
            if(i==0 || (i%7) != 0){
                cells.push(spot);
            }
            else{
                let row = cells.slice();
                rows.push(row);
                cells = [];
                cells.push(spot);
            }
            if(i == calendarSpots.length-1){
                let row = cells.slice();
                rows.push(row);
            }
        });
        let calendarRows = rows.map((row, i)=>{
            return(
                <tr key={i*99} style={{height: 83, padding: '0px', margin:0, justifyContent:"center"}}>
                    {row}
                </tr>
            );
        });
        return(
            <div style={this.calendarStyle}>
                <CalendarHeader nextMonthFunc={this.props.nextMonthFunc} prevMonthFunc={this.props.prevMonthFunc} currentMonth={this.month()} width={600}/>
                <table style={{borderSpacing: '0px', borderCollapse: 'collapse'}}>
                    <tr style={{height: 5}}>
                        {weekdays}
                    </tr>
                    <tbody style={{marginTop:0, paddingTop:0}}>
                        {calendarRows}
                    </tbody>
                </table>
            </div>
        );
    }
}