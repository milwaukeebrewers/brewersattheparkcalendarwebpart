
    export interface StatusObj {
        abstractGameState: string;
        codedGameState: string;
        detailedState: string;
        statusCode: string;
        abstractGameCode: string;
    }

    export interface LeagueRecordObj {
        wins: number;
        losses: number;
        pct: string;
    }

    export interface TeamObj {
        id: number;
        name: string;
        link: string;
    }

    export interface ProbablePitcherObj{
        id: number;
        fullName: string;
    }

    export interface AwayObj {
        leagueRecord: LeagueRecordObj;
        team: TeamObj;
        probablePitcher: ProbablePitcherObj;
        splitSquad: boolean;
        seriesNumber: number;
    }

    export interface HomeObj {
        leagueRecord: LeagueRecordObj;
        probablePitcher: ProbablePitcherObj;
        team: TeamObj;
        splitSquad: boolean;
        seriesNumber: number;
    }

    export interface TeamsObj {
        away: AwayObj;
        home: HomeObj;
    }

    export interface VenueObj {
        id: number;
        name: string;
        link: string;
    }

    export interface ContentObj {
        link: string;
    }

    export interface GamesObj {
        gamePk: number;
        link: string;
        gameType: string;
        season: string;
        gameDate: Date;
        officialDate: Date;
        status: StatusObj;
        teams: TeamsObj;
        venue: VenueObj;
        content: ContentObj;
        isTie: string;
        gameNumber: number;
        publicFacing: boolean;
        doubleHeader: string;
        gamedayType: string;
        tiebreaker: string;
        calendarEventID: Date;
        seasonDisplay: string;
        dayNight: string;
        description: string;
        scheduledInnings: number;
        reverseHomeAwayStatus: boolean;
        inningBreakLength: number;
        gamesInSeries: number;
        seriesGameNumber: number;
        seriesDescription: string;
        recordSource: string;
        ifNecessary: string;
        ifNecessaryDescription: string;
    }

    export interface DatesObj {
        date: Date;
        totalItems: number;
        totalEvents: number;
        totalGames: number;
        totalGamesInProgress: number;
        games: (GamesObj)[];
    }

    export interface EventsObj{
    
    }

    export interface GameResponse {
        copyright: string;
        totalItems: number;
        totalEvents: number;
        totalGames: number;
        totalGamesInProgress: number;
        dates: (DatesObj)[];
        events: (EventsObj)[] | null;
    }
