import * as React from 'react';
import styles from './AtTheParkCalendarWebPart.module.scss';
import {CalendarDateItem} from './ICalendarDateItem';
import {CalendarDateCardProps} from './ICalendarDateCardProps';

export default class CalendarDateCard extends React.Component<CalendarDateCardProps, {}>  
{
    
    private cardStyle={
        display: 'inline-block',
        margin: '0px',
        justifyContent: 'start',
        alignSelf: 'stretch',
        justifySelf: 'stretch',
        alignContent: 'start',
        overflow: 'hidden',
        backgroundColor:'rgba(181,146,47,.75)',
        padding: '5px 8px 8px 8px',
        height: 65,
        width: 65,
        cursor: 'default'
    };
    private dateDisplayStyle ={
        placeContent: 'start start',
        color: 'white',
        margin: '0 0 0 0',
        paddng: '0 0 0 0',
        maxWidth: 15
    };
    private gameDescriptionStyle = {
        fontSize: 12,
        color: 'white',
        margin: '0px 0px 0px 0px',
        paddng: '5px 0px 0px 0px',
        width: '100%',
        fontWeight: 500
    };
    public render(){
        this.cardStyle = {
            display: 'inline-block', 
            alignSelf: 'stretch',
            justifySelf: 'stretch',
            justifyContent: 'start',
            alignContent: 'start',  
            margin: '0px',
            overflow: 'hidden',
            cursor: (this.props.dateItem.game === undefined || this.props.dateItem.game === null) ? 'default' : 'pointer',
            backgroundColor: (this.props.dateItem.game === undefined || this.props.dateItem.game === null) ? 'white' : ((this.props.dateItem.game.isHomeGame) ? 'rgba(56,67,120)': 'rgba(181,146,47)'),
            padding: '5px 8px 8px 8px',
            height: 65,
            width: 65,
        };
        this.dateDisplayStyle ={
            placeContent: 'start start',
            color: (this.props.dateItem.game === undefined || this.props.dateItem.game === null) ? 'black' : 'white',
            margin: '0 0 0 0',
            paddng: '0 0 0 0',
            maxWidth: 15
        };
        this.gameDescriptionStyle = {
            fontSize: 12,
            color: (this.props.dateItem.game === undefined || this.props.dateItem.game === null) ? 'black' : 'white',
            margin: '2px 0px 0px 0px',
            paddng: '5px 0px 0px 0px',
            width: '100%',
            fontWeight: 500
        };
        if(this.props.dateItem.game === undefined || this.props.dateItem.game === null){
            return(
                <div style={this.cardStyle}>
                    <h4 style={this.dateDisplayStyle}>{this.props.dateItem.date.date()}</h4>
                </div>
                );
        }
        else{
            return(
                <div style={this.cardStyle} onClick={()=>this.props.selectDateFunc(this.props.dateItem.date)}>
                    <h4 style={this.dateDisplayStyle}>{this.props.dateItem.date.date()}</h4>
                    <p style={this.gameDescriptionStyle}>{this.props.dateItem.game.gameName}</p>
                    <p style={this.gameDescriptionStyle}>{this.props.dateItem.game.gameDate.format("h:mm A")}</p>
                </div>
                );
        }
    }
}