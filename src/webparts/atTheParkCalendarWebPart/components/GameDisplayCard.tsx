import * as React from 'react';
import {GameDisplayCardProps} from './IGameDisplayCardProps';
import {GameResponse} from './IGameResponse';
import {
    SPHttpClient,
    SPHttpClientResponse,
    HttpClient,
    HttpClientResponse
  } from '@microsoft/sp-http';

export default class GameDisplayCard extends React.Component<GameDisplayCardProps, {}>  
{
    constructor(props){
        super(props);

        this.state = {
            pk: this.props.game.gamePk,
            awayPitcherImage: 'https://mkebrewers.sharepoint.com/Style%20Library/Brewers%20Intranet/images/im_noplayer_90x135.gif',
            homePitcherImage: 'https://mkebrewers.sharepoint.com/Style%20Library/Brewers%20Intranet/images/im_noplayer_90x135.gif',
            homePitcher: "TBD",
            awayPitcher: "TBD",
            homeTeamLogo: "",
            awayTeamLogo: "",
            stadium: "Suprise Stadium"
        };
    }
    public componentWillMount(){
        this.props.context.httpClient.get(`https://statsapi.mlb.com/api/v1/schedule/?sportId=1&gamePk=${this.props.game.gamePk}&hydrate=probablePitcher`, HttpClient.configurations.v1)
            .then((response:HttpClientResponse) => {
                return response.json();
            }).then(res=>{
                let response : GameResponse;
                let homePitcherImage: string;
                let awayPitcherImage: string;
                let homePitcher: string;
                let awayPitcher: string;
                response = JSON.parse(JSON.stringify(res));
                if(response.dates[0].games[0].teams.home.probablePitcher){
                    homePitcherImage = `https://securea.mlb.com/mlb/images/players/head_shot/${response.dates[0].games[0].teams.home.probablePitcher.id}.jpg`;
                    homePitcher = response.dates[0].games[0].teams.home.probablePitcher.fullName;
                }
                else{
                    homePitcherImage = 'https://mkebrewers.sharepoint.com/Style%20Library/Brewers%20Intranet/images/im_noplayer_90x135.gif';
                    homePitcher = "TBD";
                }
                if(response.dates[0].games[0].teams.away.probablePitcher){
                    awayPitcherImage = `https://securea.mlb.com/mlb/images/players/head_shot/${response.dates[0].games[0].teams.away.probablePitcher.id}.jpg`;
                    awayPitcher = response.dates[0].games[0].teams.away.probablePitcher.fullName;
                }
                else{
                    awayPitcherImage = 'https://mkebrewers.sharepoint.com/Style%20Library/Brewers%20Intranet/images/im_noplayer_90x135.gif';
                    awayPitcher = "TBD";
                }
                this.setState({
                    homeTeamLogo: `https://www.mlbstatic.com/team-logos/${response.dates[0].games[0].teams.home.team.id}.svg`,
                    awayTeamLogo: `https://www.mlbstatic.com/team-logos/${response.dates[0].games[0].teams.away.team.id}.svg`,
                    homePitcherImage: homePitcherImage,
                    awayPitcherImage: awayPitcherImage,
                    awayPitcher: awayPitcher,
                    homePitcher: homePitcher,
                    stadium: response.dates[0].games[0].venue.name, 
                    pk:this.props.game.gamePk

                });

            });
    }
    public componentDidUpdate(){
        if(this.state['pk'] !== this.props.game.gamePk){
            this.props.context.httpClient.get(`https://statsapi.mlb.com/api/v1/schedule/?sportId=1&gamePk=${this.props.game.gamePk}&hydrate=probablePitcher`, HttpClient.configurations.v1)
            .then((response:HttpClientResponse) => {
                return response.json();
            }).then(res=>{
                let response : GameResponse;
                let homePitcherImage: string;
                let awayPitcherImage: string;
                let homePitcher: string;
                let awayPitcher: string;
                response = JSON.parse(JSON.stringify(res));
                if(response.dates[0].games[0].teams.home.probablePitcher){
                    homePitcherImage = `https://securea.mlb.com/mlb/images/players/head_shot/${response.dates[0].games[0].teams.home.probablePitcher.id}.jpg`;
                    homePitcher = response.dates[0].games[0].teams.home.probablePitcher.fullName;
                }
                else{
                    homePitcherImage = 'https://mkebrewers.sharepoint.com/Style%20Library/Brewers%20Intranet/images/im_noplayer_90x135.gif';
                    homePitcher = "TBD";
                }
                if(response.dates[0].games[0].teams.away.probablePitcher){
                    awayPitcherImage = `https://securea.mlb.com/mlb/images/players/head_shot/${response.dates[0].games[0].teams.away.probablePitcher.id}.jpg`;
                    awayPitcher = response.dates[0].games[0].teams.away.probablePitcher.fullName;
                }
                else{
                    awayPitcherImage = 'https://mkebrewers.sharepoint.com/Style%20Library/Brewers%20Intranet/images/im_noplayer_90x135.gif';
                    awayPitcher = "TBD";
                }
                this.setState({
                    homeTeamLogo: `https://www.mlbstatic.com/team-logos/${response.dates[0].games[0].teams.home.team.id}.svg`,
                    awayTeamLogo: `https://www.mlbstatic.com/team-logos/${response.dates[0].games[0].teams.away.team.id}.svg`,
                    homePitcherImage: homePitcherImage,
                    awayPitcherImage: awayPitcherImage,
                    awayPitcher: awayPitcher,
                    homePitcher: homePitcher,
                    stadium: response.dates[0].games[0].venue.name,
                    pk: this.props.game.gamePk
                });

            });
        }
    }
    public render(){
        if(this.props.game === null || this.props.game === undefined){
            return(
                <div style={{height: 225, width: 625, background: 'white', marginBottom: 10, boxShadow: "0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1)", borderRadius:"4px"}}>
                    <div style={{height: 50, width: 625, background: '#0a2351', margin: 0, textAlign:'center', justifyContent:'center'}}>
                        <h3> No Game Today</h3>
                    </div>
                </div>
            );
        }
        else if(!this.props.game.isHomeGame){
            let gameDescription = this.props.game.gameName;
            return(
                <div style={{height: 370, width: 625, background: 'white', marginBottom: 10, boxShadow: "0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1)", borderRadius:'4px'}}>
                    <div style={{height: 60, width: 625, background: 'rgba(181,146,47)', margin: 0, padding:0, textAlign:'center', justifyContent:'center'}}>
                        <h3 style={{paddingBottom: 0, marginBottom: 0}}>{this.props.game.gameName}</h3>
                        <h4 style={{paddingTop: 0, marginTop: 0, paddingBottom: 0, marginBottom: 0}}>{this.props.game.gameDate.format("MMMM Do, YYYY")}</h4>
                        <h4 style={{paddingTop: 0, marginTop: 0}}>{this.state['stadium']}</h4>
                    </div>
                    <div style={{display: 'inline-flex', alignContent: 'space-around', justifyContent:'space-around', width: 625}}>
                        <div style={{textAlign:'center', justifyContent:'center', padding: "10px", flexDirection:'column'}}>
                            <img style={{height:100, width:80, padding:5}} src={this.state['awayTeamLogo']}/>
                            <br/>
                            <img style={{ maxHeight:135, maxWidth:90}} src={this.state['awayPitcherImage']}/>
                            <h4 style={{color:'black'}}>{this.state['awayPitcher']}</h4>
                        </div>
                        <div style={{textAlign:'center', justifyContent:'center', alignContent:'center', paddingTop: 75}}>
                            <h4 style={{color:'black'}}>{this.props.game.time}</h4>
                        </div>
                        <div style={{textAlign:'center', justifyContent:'center', padding: "10px", flexDirection:'column'}}>
                            <img style={{height:100, width:80, padding:5}} src={this.state['homeTeamLogo']}/>
                            <br/>
                            <img style={{height:135, width:90}} src={this.state['homePitcherImage']}/>
                            <h4 style={{color:'black'}}>{this.state['homePitcher']}</h4>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            let gameDescription = this.props.game.gameDescription.split("; ").map((item, i)=>{
                return(<span key={i}>
                {item}
                <br/>
                </span>);
            });
            let firstPitch = (this.props.game.firstPitch !== null && this.props.game.firstPitch !== undefined) ? this.props.game.firstPitch.split(/[0-9]/).map((item, i)=>{
                if(i !== 0 ){
                    return(<span key={i*23}>
                        {`${i}`}{item}
                        <br/>
                        </span>);
                }
            }) : null;
            let presentations = (this.props.game.presentations !== null && this.props.game.presentations !== undefined) ? this.props.game.presentations.split(/[0-9]/).map((item, i)=>{
                if(i !== 0 ){
                    return(<span key={i*43}>
                        {`${i}`}{item}
                        <br/>
                        </span>);
                }
            }) : null;
            return(
                <div style={{height: 370, width: 625, background: 'white', marginBottom: 10, boxShadow: "0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1)", borderRadius:'4px'}}>
                    <div style={{height: 60, width: 625, background: '#0a2351', margin: 0, textAlign:'center', justifyContent:'center'}}>
                        <h3 style={{paddingBottom: 0, marginBottom: 0}}>{this.props.game.gameName}</h3>
                        <h4 style={{paddingTop: 0, marginTop: 0, paddingBottom: 0, marginBottom: 0}}>{this.props.game.gameDate.format("MMMM Do, YYYY")}</h4>
                        <h4 style={{paddingTop: 0, marginTop: 0}}>{this.state['stadium']}</h4>
                    </div>
                    <div style={{display: 'flex'}}>
                        <div style={{textAlign:'center', justifyContent:'center', padding: "10px", flexDirection:'column'}}>
                            <img style={{height:100, width:80, padding:5}} src={this.state['awayTeamLogo']}/>
                            <br/>
                            <img style={{height:135, width:90}} src={this.state['awayPitcherImage']}/>
                            <h4 style={{color:'black'}}>{this.state['awayPitcher']}</h4>
                        </div>
                        <div style={{flex:1, paddingLeft:10, paddingRight:5, overflow: "hidden"}}>
                            <p style={{color: 'black'}}><span style={{fontWeight:500}}>{gameDescription}</span></p>
                            <p style={{color: 'black'}}><span style={{fontWeight:500}}>National Anthem: </span>{this.props.game.nationalAnthem}</p>
                            <p style={{color: 'black'}}><span style={{fontWeight:500}}>Color Guard: </span>{this.props.game.colorGuard}</p>
                        </div>
                        <div style={{flex:1, paddingLeft:10, paddingRight:5, borderColor: 'black', borderLeftWidth: 1}}>  
                            <p style={{color: 'black'}}><span style={{fontWeight:500}}>First Pitch: <br/></span>{firstPitch}</p>
                            <p style={{color: 'black'}}><span style={{fontWeight:500}}>Play Ball Kid: </span>{this.props.game.playBallKid}</p>
                        </div>
                        <div style={{flex:1}}>
                            <p style={{color: 'black'}}><span style={{fontWeight:500}}>Presentations:<br/> </span>{presentations}</p>
                        </div>
                        <div style={{textAlign:'center', justifyContent:'center', padding: "10px", flexDirection:'column'}}>
                            <img style={{height:100, width:80, padding:5}} src={this.state['homeTeamLogo']}/>
                            <br/>
                            <img style={{height:135, width:90}} src={this.state['homePitcherImage']}/>
                            <h4 style={{color:'black'}}>{this.state['homePitcher']}</h4>
                        </div>
                    </div>
                </div>
            );
        }
    }
}