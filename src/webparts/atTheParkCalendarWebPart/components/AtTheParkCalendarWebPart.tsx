import * as React from 'react';
import * as moment from 'moment';
import styles from './AtTheParkCalendarWebPart.module.scss';
import { IAtTheParkCalendarWebPartProps } from './IAtTheParkCalendarWebPartProps';
import { SharePointGame } from './ISharePointGame';
import { Game } from './IGame';
import {CalendarDateItem} from './ICalendarDateItem';
import Calendar from './Calendar';
import GameDisplayCard from './GameDisplayCard';
import {MLBGameDate, GamesEntity, Status, TeamOrVenue, Teams, LeagueRecord, AwayOrHome, Content} from './IMLBGame';
import { escape } from '@microsoft/sp-lodash-subset';

export default class AtTheParkCalendarWebPart extends React.Component<IAtTheParkCalendarWebPartProps, {selectedDate: moment.Moment, gameList: Game[], calendarList: CalendarDateItem[], selectedGame: Game}> {
  public constructor(props){
    super(props);
  }
  private _initalizeData(){
    let gameList = this._initializeGameList();
    let date = moment();
    let calendarList: CalendarDateItem[] = [];
    let selectedMonth = date.month();
    let selectedGame: Game;
    let monthCount: number = date.daysInMonth();
    let monthGameList = gameList.filter((game)=>{
      return date.isSame(game.gameDate, 'month');
    });
    for(let i = 1; i <= monthCount; i++ ){
      let newDate = moment(date).set("date", i);
      let games = monthGameList.filter((g)=>{
        return newDate.isSame(g.gameDate, 'day');
      });
      let game = (games == undefined || games == null || games.length <= 0) ? null : games[0];
      if(game != null && game.gameDate.isSame(date, "day") && game.isHomeGame){
        selectedGame = game;
      }
      let newItem: CalendarDateItem = {
        date: newDate,
        game: game,
      };
      calendarList.push(newItem);
    }    
    if(selectedGame == null || selectedGame == undefined){
      selectedGame = gameList.filter((x)=>{return date.isBefore(x.gameDate);}).sort((x, y)=>x.gameDate.valueOf() - y.gameDate.valueOf())[0];
    }
    this.setState({
      selectedDate: date,
      selectedGame: selectedGame,
      gameList: gameList,
      calendarList: calendarList
    });
  }

  private _initializeGameList(){
    let newGameList = this.props.sharePointGames.map((spGame)=>{
      let game: Game  = {
        colorGuard: spGame.ColorGuard,
        firstPitch: spGame.FirstPitch,
        gameDate: moment(spGame.GameDate),
        gameDescription: spGame.GameType,
        gameName: spGame.o3e4,
        isHomeGame: true,
        nationalAnthem: spGame.Title,
        playBallKid: spGame.ohbg,
        presentations: spGame.Presentations,
        scoreboard: spGame.Scoreboard,
        gamePk: spGame.gamePk
      };
      return game;
    });
    this.props.awayGames.forEach(game => {
      newGameList.push(game);
    });
    return newGameList;
  }

  public componentWillMount(){
    this._initalizeData();
  }
  private generateCalendarList(date: moment.Moment){
    let calendarList: CalendarDateItem[] = [];
    let selectedMonth = date.month();
    let monthCount: number = date.daysInMonth();
    let monthGameList = this.state.gameList.filter((game)=>{
      return date.isSame(game.gameDate, 'month');
    });
    for(let i = 1; i <= monthCount; i++ ){
      let newDate = moment(date).set("date", i);
      let games = monthGameList.filter((g)=>{
        return newDate.isSame(g.gameDate, 'day');
      });
      let game = (games == undefined || games == null || games.length <= 0) ? undefined : games[0];
      let newItem: CalendarDateItem = {
        date: newDate,
        game: game,
      };
      calendarList.push(newItem);
    }
    return calendarList;
  }
  private nextMonth()
  {
    let date = this.state.selectedDate;
    date = moment(date).add(1, "month");
    let newCalendarList = this.generateCalendarList(date);
    this.setState({
      selectedDate: date,
      calendarList: newCalendarList
    });
  }
  private prevMonth()
  {
    let date = this.state.selectedDate;
    date = moment(date).subtract(1, "month");
    let newCalendarList = this.generateCalendarList(date);
    this.setState({
      selectedDate: date,
      calendarList: newCalendarList
    });
  } 
  private selectDate(date: moment.Moment){
    let games = this.state.gameList.filter((x)=>{
      return date.isSame(x.gameDate, 'day');
    });
    let game = (games == undefined || games == null || games.length <= 0) ? undefined : games[0];
    this.setState({
      selectedGame: game,
      selectedDate: date
    });
  }
  public render(): React.ReactElement<IAtTheParkCalendarWebPartProps> {
    return (
      <div className={ styles.atTheParkCalendarWebPart }>
        <div className={ styles.container }>
          <div className={ styles.row }>
            <div className={ styles.column }>
              <GameDisplayCard game={this.state.selectedGame} context={this.props.context}/>
              <Calendar nextMonthFunc={this.nextMonth.bind(this)} prevMonthFunc={this.prevMonth.bind(this)} selectedDate={this.state.selectedDate} calendarList={this.state.calendarList} selectDateFunc={this.selectDate.bind(this)}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
