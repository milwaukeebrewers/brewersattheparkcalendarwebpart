export interface SharePointGame{
    ColorGuard?: string;          // ColorGuard for the game
    FirstPitch?: string;          // First pitch for the game
    GameDate: Date;               // Date of the game
    GameType: string;             // Time of game and any theme
    o3e4?: string;                // Day of week the game is on and opponent
    ohbg?: string;                // Play ball kid for the game
    Presentations?: string;       // Presentations for the game
    Scoreboard?: string;          // Who is doing scoreboard for the game
    Title?: string;               // National anthem singer for the game
    gamePk?: number;              // Game ID
  }