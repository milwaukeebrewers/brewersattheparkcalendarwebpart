import { Game } from './IGame';
import * as moment from 'moment';

export interface CalendarDateItem{
    date: moment.Moment;
    game?: Game;
}