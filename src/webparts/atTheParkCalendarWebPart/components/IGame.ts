import * as moment from 'moment';

export interface Game{
    colorGuard?: string;
    firstPitch?: string;
    gameDate?: moment.Moment;
    gameDescription?: string;
    gameName?: string;
    isHomeGame: boolean;
    nationalAnthem?: string;
    playBallKid?: string;
    presentations?: string;
    scoreboard?: string;
    time?: string;
    gamePk?: number;
}