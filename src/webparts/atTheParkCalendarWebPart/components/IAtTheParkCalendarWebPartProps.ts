import {SharePointGame} from './ISharePointGame';
import{Game} from'./IGame';
import { WebPartContext } from '@microsoft/sp-webpart-base';

export interface IAtTheParkCalendarWebPartProps {
  description: string;
  date: Date;
  day: number;
  sharePointGames: SharePointGame[];
  awayGames: Game[];
  month: number;
  year: number;
  context?: WebPartContext;
}
