import * as React from 'react';
import styles from './AtTheParkCalendarWebPart.module.scss';
import {CalendarHeaderProps} from './ICalendarProps';

export default class CalendarHeader extends React.Component<CalendarHeaderProps, {}>  
{
    private headerStyle = {
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#0a2351',
        width: this.props.width
    };
    private buttonStyle = {
        backgroundColor: '#0a2351',
        borderColor: 'white',
        borderWidthe: 0,
        color: 'white',
        margin: 15,
        paddingBottom: 5,
    };
    public render(){
        return(
        <div style={this.headerStyle}>
            <h3 style={{flex:3, paddingLeft:30}}>{this.props.currentMonth}</h3>
           <div style={{flex:1}}>
                <button onClick={this.props.prevMonthFunc} style={this.buttonStyle}> {"<"} </button>
                <button onClick={this.props.nextMonthFunc} style={this.buttonStyle}> {">"} </button>
           </div>
        </div>
        );
    }
}