import {CalendarDateItem} from './ICalendarDateItem';
import * as moment from 'moment';

export interface CalendarProps
{
    nextMonthFunc: () => void;
    prevMonthFunc?: () => void;
    selectDateFunc?: (date:  moment.Moment) => void;
    calendarList: CalendarDateItem[];
    selectedDate?: moment.Moment;
    days?: CalendarDateItem[];
}
export interface CalendarHeaderProps
{
    nextMonthFunc: () => void;
    prevMonthFunc?: () => void;
    currentMonth: string;
    width:number;
}