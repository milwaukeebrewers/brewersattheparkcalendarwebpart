import { CalendarDateItem } from './ICalendarDateItem';
import * as moment from 'moment';

export interface CalendarDateCardProps{
    selectDateFunc?: (date:  moment.Moment) => void;
    dateItem: CalendarDateItem;
}