import {Game} from './IGame';
import { WebPartContext } from '@microsoft/sp-webpart-base';
export interface GameDisplayCardProps{
    game?: Game;
    context?: WebPartContext;
}