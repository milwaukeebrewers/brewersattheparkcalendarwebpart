export interface MLBAPIResponse{
    copyright: string;
    totalItems: number;
    totalEvents: number;
    totalGames: number;
    totalGamesInProgress: number;
    dates?: (MLBGameDate)[] | null;
}
export interface MLBGameDate {
    date: string;
    totalItems: number;
    totalEvents: number;
    totalGames: number;
    totalGamesInProgress: number;
    games?: (GamesEntity)[] | null;
    events?: (null)[] | null;
  }
  export interface GamesEntity {
    gamePk: number;
    link: string;
    gameType: string;
    season: string;
    gameDate: string;
    status: Status;
    teams: Teams;
    venue: TeamOrVenue;
    content: Content;
    gameNumber: number;
    publicFacing: boolean;
    doubleHeader: string;
    gamedayType: string;
    tiebreaker: string;
    calendarEventID: string;
    seasonDisplay: string;
    dayNight: string;
    scheduledInnings: number;
    inningBreakLength: number;
    gamesInSeries: number;
    seriesGameNumber: number;
    seriesDescription: string;
    recordSource: string;
    ifNecessary: string;
    ifNecessaryDescription: string;
  }
  export interface Status {
    abstractGameState: string;
    codedGameState: string;
    detailedState: string;
    statusCode: string;
    abstractGameCode: string;
  }
  export interface Teams {
    away: AwayOrHome;
    home: AwayOrHome;
  }
  export interface AwayOrHome {
    leagueRecord: LeagueRecord;
    team: TeamOrVenue;
    splitSquad: boolean;
    seriesNumber: number;
  }
  export interface LeagueRecord {
    wins: number;
    losses: number;
    pct: string;
  }
  export interface TeamOrVenue {
    id: number;
    name: string;
    link: string;
  }
  export interface Content {
    link: string;
  }
  