import * as React from 'react';
import * as ReactDom from 'react-dom';
import * as moment from 'moment';

import { Version } from '@microsoft/sp-core-library';

import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import {
  SPHttpClient,
  SPHttpClientResponse,
  HttpClient,
  HttpClientResponse
} from '@microsoft/sp-http';

import {MLBGameDate, GamesEntity, Status, TeamOrVenue, Teams, LeagueRecord, AwayOrHome, Content, MLBAPIResponse} from './components/IMLBGame';
import {Game} from './components/IGame';
import * as strings from 'AtTheParkCalendarWebPartWebPartStrings';
import AtTheParkCalendarWebPart from './components/AtTheParkCalendarWebPart';
import { IAtTheParkCalendarWebPartProps } from './components/IAtTheParkCalendarWebPartProps';
import {SharePointGame} from './components/ISharePointGame';

export interface IAtTheParkCalendarWebPartWebPartProps {
  description: string;
}

export default class AtTheParkCalendarWebPartWebPart extends BaseClientSideWebPart<IAtTheParkCalendarWebPartWebPartProps> {
  private gamesList: SharePointGame[];
  private awayGamesList: Game[];
  private homeGamesList: Game[];
  private mlbGamesResponse: MLBAPIResponse;
  public onInit(): Promise<void>{
    return this._retrieveSharePointGames()
    .then((response)=>{
      this.gamesList = response.value.map(x=>{
        return this._saturateMLBDataForHomeGame(x);
      });
    }).then((r)=>{
      let dates = this.gamesList.map(x=>{
        return x.GameDate;
      });
      let unmatchedGames = this.homeGamesList.filter(game =>{
        for(let i =0; i < dates.length; i++){
          if(moment(game.gameDate).isSame(moment(dates[i]), "day")){
            return false;
          }
        }
        return true;
      });
      for(let j = 0; j < unmatchedGames.length; j++){
        this.gamesList.push({
          GameDate: new Date(unmatchedGames[j].gameDate.toISOString()),
          GameType: `${unmatchedGames[j].time}; ${unmatchedGames[j].gameDescription}`,
          gamePk: unmatchedGames[j].gamePk,
          o3e4: unmatchedGames[j].gameName,
        });
      }
    });
  }
  private async _retrieveSharePointGames(){
    let item:MLBAPIResponse;
    await this._retrieveMLBGames().then((mlbresponse)=>{
      this.mlbGamesResponse = mlbresponse;
      item = JSON.parse(JSON.stringify(mlbresponse));

      this.homeGamesList = item.dates.filter((game)=>
      {
        return !this._checkIfHomeGame(game);
      }).map(x=>this._convertMLBGame(x));

      item.dates = item.dates.filter((game)=>
      {
        return this._checkIfHomeGame(game);
      });
      this.awayGamesList = item.dates.map((date)=>{
        return this._convertMLBGame(date);
      });
    });
    let thing = moment().startOf("year").toISOString();
    return this.context.spHttpClient.get(`${this.context.pageContext.web.absoluteUrl}/_api/lists/GetByTitle('\Field Activities')/items?$Select=GameDate,GameType,Title,o3e4,ColorGuard,Presentations,FirstPitch,Scoreboard,ohbg&$filter=GameDate ge datetime'${thing}'&$Top=200`,  
    SPHttpClient.configurations.v1)  
    .then((response: SPHttpClientResponse) => {
      return response.json();
    });
  }
  private _saturateMLBDataForHomeGame(game: SharePointGame){
    if(this.mlbGamesResponse == null || this.mlbGamesResponse == undefined){
      return game;
    }
    else{
      let matchingGames = this.mlbGamesResponse.dates.filter(x=>moment(game.GameDate).isSame(moment(x.date),"day"));
      let matchingGame;
      if(matchingGames){
        matchingGame = matchingGames[0];
      }
      else{
        return game;
      }
      game.GameDate = new Date(matchingGame.games[0].gameDate);
      game.gamePk = matchingGame.games[0].gamePk;
      return game;
    }
  }
  private _checkIfHomeGame(game:MLBGameDate){
    let isHome: boolean = false;
    let filteredList = game.games.filter((x)=>{
      return x.teams.home.team.id == 158;
    });
    isHome = filteredList.length > 0;
    if(isHome){
      return false;
    }
    else{
      return true;
    }
  }
  private _convertMLBGame(game: MLBGameDate): Game{
    let date = moment(game.games[0].gameDate);
    let weekdays= moment.weekdaysShort();
    let weekday = weekdays[date.weekday()];
    let isAwayGame = this._checkIfHomeGame(game);
    let subGame = isAwayGame ? game.games.filter(x=>x.teams.away.team.id == 158)[0] : game.games.filter(x=>x.teams.home.team.id == 158)[0];    
    let gameName: string = isAwayGame ? `${weekday} @ ${subGame.teams.home.team.name}` : `${weekday} vs ${subGame.teams.away.team.name}`;
    return {
      gameDate: date,
      isHomeGame: !isAwayGame,
      gameName: gameName,
      time: moment(subGame.gameDate).format("h:mm A zz"),
      gamePk: subGame.gamePk,
      gameDescription: subGame.seriesDescription,
    };
  }
  private _retrieveMLBGames(){
    let startDate = moment().startOf("year").format("M/D/YYYY");
    let endDate = moment().endOf("year").add(2, "year").format("M/D/YYYY");
    return this.context.httpClient.get(`https://statsapi.mlb.com/api/v1/schedule/?sportId=1&teamId=158&startDate=${startDate}&endDate=${endDate}`, HttpClient.configurations.v1)
    .then((response:HttpClientResponse) => {
      return response.json();
    });
  }
  public render(): void {
    
    const element: React.ReactElement<IAtTheParkCalendarWebPartProps > = React.createElement(
      AtTheParkCalendarWebPart,
      {
        description: this.properties.description,
        date: new Date(),
        sharePointGames: this.gamesList,
        context: this.context,
        awayGames: this.awayGamesList,
        month: new Date().getMonth(),
        year: new Date().getFullYear(), 
        day: new Date().getDate(),
      }
    );

    ReactDom.render(element, this.domElement);
  }
  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
